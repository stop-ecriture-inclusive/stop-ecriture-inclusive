# Cette page contient des exemples d’écriture inclusive. Consultez-la avec l’extension et vous ne devriez plus voir d’écriture inclusive du tout.

## Avec les points médians :
### « · »
Jean-Michel le directeur·rice arrive ce matin à Saint-Denis, car iel a une annonce à faire. Soyez gentils·les !

Les be·aux·lles ambassadeur·rice·s sont arrivé·e·s avec les chef·fe·s ce matin, car illes doivent rencontrer les lecteur·rice·s français·e·s qui sont divisé·e·s.

Cher·e·s copaines, quelqu’un·e serait-il·elle prêt·e à m’aider pour mon déménagement ?

Ce sont des jume·aux·lles, et iel en est fier·ière !

Les représentant·e·s syndicaux·ales sont en route.

### « • »
Jean-Michel le directeur•rice arrive ce matin à Saint-Denis, car iel a une annonce à faire. Soyez gentils•les !

Les be•aux•lles ambassadeur•rice•s sont arrivé•e•s avec les chef•fe•s ce matin, car illes doivent rencontrer les lecteur•rice•s français•e•s qui sont divisé•e•s.

Cher•e•s copaines, quelqu’un•e serait-il•elle prêt•e à m’aider pour mon déménagement ?

Ce sont des jume•aux•lles, et iel en est fier•ière !

Les représentant•e•s syndicaux•ales sont en route.

### « ⋅ »
Jean-Michel le directeur⋅rice arrive ce matin à Saint-Denis, car iel a une annonce à faire. Soyez gentils⋅les !

Les be⋅aux⋅lles ambassadeur⋅rice⋅s sont arrivé⋅e⋅s avec les chef⋅fe⋅s ce matin, car illes doivent rencontrer les lecteur⋅rice⋅s français⋅e⋅s qui sont divisé⋅e⋅s.

Cher⋅e⋅s copaines, quelqu’un⋅e serait-il⋅elle prêt⋅e à m’aider pour mon déménagement ?

Ce sont des jume⋅aux⋅lles, et iel en est fier⋅ière !

Les représentant⋅e⋅s syndicaux⋅ales sont en route.

### « ‧ »
Jean-Michel le directeur‧rice arrive ce matin à Saint-Denis, car iel a une annonce à faire. Soyez gentils‧les !

Les be‧aux‧lles ambassadeur‧rice‧s sont arrivé‧e‧s avec les chef‧fe‧s ce matin, car illes doivent rencontrer les lecteur‧rice‧s français‧e‧s qui sont divisé‧e‧s.

Cher‧e‧s copaines, quelqu’un‧e serait-il‧elle prêt‧e à m’aider pour mon déménagement ?

Ce sont des jume‧aux‧lles, et iel en est fier‧ière !

Les représentant‧e‧s syndicaux‧ales sont en route.

## Avec le point normal « . »
Jean-Michel le directeur.rice arrive ce matin à Saint-Denis, car iel a une annonce à faire. Soyez gentils.les !

Les be.aux.lles ambassadeur.rice.s sont arrivé.e.s avec les chef.fe.s ce matin, car illes doivent rencontrer les lecteur.rice.s français.e.s qui sont divisé.e.s.

Cher.e.s copaines, quelqu’un.e serait-iel prêt.e à m’aider pour mon déménagement ?

Ce sont des jume.aux.lles, et iel en est fier.ière !

Les représentant.e.s syndicaux.ales sont en route.

## Avec le tiret « - »
Jean-Michel le directeur-rice arrive ce matin à Saint-Denis, car iel a une annonce à faire. Soyez gentils-les !

Les be-aux-lles ambassadeur-rice-s sont arrivé-e-s avec les chef-fe-s ce matin, car illes doivent rencontrer les lecteur-rice-s français-e-s qui sont divisé-e-s.

Cher-e-s copaines, quelqu’un-e serait-iel prêt-e à m’aider pour mon déménagement ?

Ce sont des jume-aux-lles, et iel en est fier-ière !

Les représentant-e-s syndicaux-ales sont en route.

# Autres exemples
L’instituteurice sera en retard.

# Exemples ne devant pas être modifiés
E.Leclerc a des liens avec Paris-Est.
const CopyWebpackPlugin = require('copy-webpack-plugin');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
  entry: {
    content_script: __dirname + '/src/content-script.js',
    background_script: __dirname + '/src/background-script.js',
    popup_script: __dirname + '/src/popup/popup-script.js'
  },
  resolve: {
    modules: ['./src', './node_modules'],
    alias: {
      XRegExp: 'XRegExp'
    }
  },
  optimization: {
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          output: {
            ascii_only: true // required for Chrome, increases the size of the regexps by about 15%
          }
        }
      })
    ]
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {from: 'node_modules/webextension-polyfill/dist/browser-polyfill.js'},
        {from: 'src/popup/popup.html'},
        {from: 'src/popup/popup-style.css'},
      ],
    })
  ]
};

const CopyWebpackPlugin = require('copy-webpack-plugin');
const path = require('path');

module.exports = {
  name: "firefox",
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist/firefox/')
  },
  plugins: [
    new CopyWebpackPlugin({
      patterns: [
        {from: 'firefox-manifest.json', to: 'manifest.json'},
        {from: 'icons/svg', to: 'icons/'}
      ],
    })
  ]
};

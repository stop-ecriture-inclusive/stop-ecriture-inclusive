var DEBUG = { log: false, timer: false };

/**
 * @method isExtensionEnabled
 * @return {Boolean}
 */
function isExtensionEnabled() {
  DEBUG.log && console.log('isExtensionEnabled: ', localStorage.getItem('extensionStatus') === 'enabled');
  return localStorage.getItem('extensionStatus') === 'enabled';
}

/**
 * @method setExtensionStatus
 * @param  {String}           extensionStatus either 'enabled' or 'disabled'
 * @return {undefined}
 */
function setExtensionStatus(extensionStatus) {
  return localStorage.setItem('extensionStatus', extensionStatus);
}

/**
 * @method setDebugOptions
 * @param  {Object}        debugOpions
 * @return {Object}
 */
function setDebugOptions(debugOpions) {
  localStorage.setItem('debugOpions', JSON.stringify(debugOpions));
  return DEBUG = debugOpions;
}

/**
 * @method getDebugOptions
 * @return {Object|undefined}
 */
function getDebugOptions() {
  return JSON.parse(localStorage.getItem('debugOpions'));
}

/**
 * Set browserAction icon depending on the browser as Chrome doesn't support SVG
 * @method setBrowserActionIcon
 * @param  {Boolean}            isEnabled
 */
// function setBrowserActionIcon(isEnabled) {
//   let iconToUse = isEnabled ? 'stop-ecriture-inclusive' : 'stop-ecriture-inclusive-bw';
//   // window.navigator.vendor will be empty on Firefox,
//   // see https://developer.mozilla.org/en-US/docs/Web/API/Navigator/vendor
//   iconToUse += window.navigator.vendor.length ? '.png' : '.svg';
//   browser.browserAction.setIcon({path: `icons/${iconToUse}`});
// }

setExtensionStatus('enabled');
var DEBUG = getDebugOptions() ?? setDebugOptions({ log: false, timer: false });

browser.runtime.onMessage.addListener(
  (data, _sender) => {
    DEBUG.log && console.log('resolving promise data:', data);
    switch (data.request) {
      case 'contentScriptData':
      case 'popupData':
        return Promise.resolve(
          {
            isExtensionEnabled: isExtensionEnabled(),
            debugOpions: getDebugOptions()
          }
        );

      case 'toggleExtensionIsEnabled':
        DEBUG.log && console.log(data);
        setExtensionStatus(data.isEnabled ? 'enabled' : 'disabled');
        // setBrowserActionIcon(data.isEnabled);
        browser.tabs.reload();
        break;

      case 'updateDebugOptions':
        setDebugOptions(data.debugOpions);
        browser.tabs.reload();
        break;
    }
  }
);

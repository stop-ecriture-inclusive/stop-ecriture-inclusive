import XRegExp from 'xregexp'
import replaceEachRegexpsArray from './regexps.js'

var isExtensionEnabled = true;
var DEBUG = { log: false, timer: false };

var timer = function(name) {
    let start = new Date();
    return {
        stop: function() {
            let end  = new Date();
            let time = end.getTime() - start.getTime();
            console.log('Timer:', name, 'finished in', time, 'ms');
        }
    }
};

browser.runtime.onMessage.addListener(
  (data, _sender) => {
    if (data.type === 'debug') {
      DEBUG = data.content;
    }
  }
);

/**
 * retrieve data from background script
 * @method requestContentScriptDataFromBackground
 * @return {Promise}
 */
function requestContentScriptDataFromBackground() {
  return browser.runtime.sendMessage({
    request: 'contentScriptData'
  }).then(
    async response => {
      isExtensionEnabled = await response.isExtensionEnabled;
      DEBUG = await response.debugOpions;
    },
    error => console.error('Error retrieving background state:', error)
  );
}

/**
 * Find the text nodes under a node and return them as an array
 * @method textNodesUnder
 * @param  {Node}       node The node to search text nodes in
 * @return {Array}           An array of the text nodes under the node parameter
 */
function textNodesUnder(node) {
  let n, a=[], walk=document.createTreeWalker(
    node,
    NodeFilter.SHOW_TEXT,
    {
      acceptNode: node => acceptNodeFilter(node)
    },
    false);
  while(n=walk.nextNode()) a.push(n);
  return a;
}

/**
 * Check if the text's parentNode is not to reject or to skip
 * @method acceptNodeFilter
 * @param  {Node}         node The node to check
 * @return {unsigned short}
 */
function acceptNodeFilter(node) {
  const nodesToReject = ['svg', 'script', 'style', 'noscript', 'iframe', 'input'];
  // const nodesToSkip = [/*'div'*/];
  const parentNodeName = node.parentNode !== null && node.parentNode.nodeName.toLowerCase()

  if(nodesToReject.includes(parentNodeName)) {
    return NodeFilter.FILTER_REJECT
  }
  if(hasAnyContentEditableParent(node)) {
    return NodeFilter.FILTER_REJECT
  }
  // if(nodesToSkip.indexOf(nodeName) > -1) {
  //   return NodeFilter.FILTER_SKIP
  // }
  return NodeFilter.FILTER_ACCEPT
}

/**
 * Returns true if the node is contenteditable
 * @method isContentEditable
 * @param {Node}        node
 * @return {boolean}
 */
function isContentEditable(node) {
  if (node.nodeType === Node.ELEMENT_NODE) {
    const valuesToAvoid = ['true', 'plaintext-only']
    const contenteditable = node.attributes.getNamedItem("contenteditable")

    return contenteditable !== null && valuesToAvoid.includes(contenteditable.value)
  }
  return false
}

/**
 * Checks recursively the parents of the node
 * Returns true if any of the parents is contenteditable
 * @method isParentContentEditable
 * @param {Node}        node
 * @return {boolean}
 */
function hasAnyContentEditableParent(node) {
  let hasReachedDocumentTop = false
  while (!hasReachedDocumentTop) {
    if (node.parentNode !== null && isContentEditable(node.parentNode)) {
      return true
    }
    node = node.parentNode
    hasReachedDocumentTop = node.parentNode === null
  }
  return false
}

/**
 * Use the regexps to replace each occurrence in a node's textContent
 * @method fixTextNode
 * @param  {Text}    textNode
 */
function fixTextNode(textNode) {
  let originalText = textNode.textContent;
  let fixedText = XRegExp.replaceEach(textNode.textContent, replaceEachRegexpsArray);
  if(originalText === fixedText) {
    return
  }
  textNode.textContent = fixedText;
  DEBUG.log && console.log(
    {
      original: originalText,
      fixed: fixedText,
      node: textNode
    }
  );
}

/**
 * Retrieves the text nodes under a node calling textNodesUnder()
 * Then, foreach text node, call fixTextNode()
 * @method fixTextsUnderNode
 * @param  {Node}          node
 */
function fixTextsUnderNode(node) {
  let textNodesUnderTimer = DEBUG.timer ? timer('textNodesUnder()') : undefined;
  let pageTextNodes = textNodesUnder(node);
  DEBUG.timer && textNodesUnderTimer.stop();

  let foreachFixTextTimer = DEBUG.timer ? timer('foreachFixText') : undefined;
  pageTextNodes.forEach(textNode => fixTextNode(textNode));
  DEBUG.timer && foreachFixTextTimer.stop();
}

/**
 * Check the type of a mutationRecord
 * If the type is 'childList', foreach modified/added node, call fixTextsUnderNode()
 * If the type is 'characterData', and if it passes the filter, call fixTextNode()
 * @method processMutationRecord
 * @param  {MutationRecord}              mutationRecord is a Dom mutation
 */
function processMutationRecord(mutationRecord) {
  switch (mutationRecord.type) {
    case 'childList':
      mutationRecord.addedNodes.forEach(node => fixTextsUnderNode(node));
      break;

    case 'characterData':
      if(acceptNodeFilter(mutationRecord.target) === NodeFilter.FILTER_ACCEPT) {
        fixTextNode(mutationRecord.target);
      }
      break;
  }
}

function setBodyObserver() {
  const bodyObserver = new MutationObserver(mutationsList => {
    mutationsList.forEach((mutationRecord) => {
      processMutationRecord(mutationRecord);
    });
  });
  bodyObserver.observe(document.body, {attributes: false, childList: true, subtree: true, characterData: true});
}

window.addEventListener('DOMContentLoaded', _event => {
  requestContentScriptDataFromBackground().then(
    () => {
      if(isExtensionEnabled) {
        fixTextsUnderNode(document.body);
        setBodyObserver();
      }
    }
  );
});

import XRegExp from 'xregexp'

const separators = ['-', '⋅', '·', '•', '‧', '\\.']
const patternSeparators = '('+separators.join('|')+')'
const startBoundary = '(?<=^|\\P{Latin})';
const endBoundary = '(?!-)(?=\\P{Latin}|$)';

/**
 * @method generateSuffixesListWithSeparators
 * @param {String}              firstSuffix
 * @param {String}              secondSuffix
 * @return {array}              A list of suffixes with separators in every combination possible
 */
function generateSuffixesListWithSeparators(firstSuffix, secondSuffix) {
  let list = []
  separators.forEach(separator => {
    let toReplace = [firstSuffix, separator, secondSuffix]
    list.push([toReplace.join(''), toReplace.reverse().join('')])
  })
  return list.flat()
}

/**
 * @method repeatPatternForAllSeparators
 * @params {...String}          Patterns to be repeated with a coma as separator placeholder
 * @return {RegExp}
 */
function repeatPatternForAllSeparators(...patterns) {
  let list = []
  patterns.forEach(pattern => {
    separators.forEach(separator => {
      list.push(pattern.replaceAll(',', separator) + endBoundary)
    })
  })
  return XRegExp(list.join('|'), 'mgiu')
}

/**
 * generate the regexps array and their replace word
 * @method generateReplaceEachRegexpsArray
 * @return {array}             An array of regexps and their replace word
 */
function generateReplaceEachRegexpsArray() {
  const wordFilters = {
    mien: ['miem', 'miæn'],
    tous: ['touste', 'toustes', 'touxe', 'touxes'],
    celui: ['cellui', 'célui'],
    ceux: ['celleux', 'ceulles', 'ceuxes', 'ceuze'],
    eux: ['elleux', 'euxes', /*'euze', 'eus'*/],
    //mon: ['maon'],
    //ton: [/*'tan',*/ 'taon'],
    //son: ['saon'],
    ce: ['cès', 'cèx'],
    un: [/*'um',*/ 'unæ'],
    //du: ['di'],
    //le: ['lia'/*, 'li', 'lo'*/],
    ils: [
      'illes',
      ...generateSuffixesListWithSeparators("ils", "elles"),
      /*'uls', 'ols', 'yels',*/ 'iels', 'ielles', /*'ims', 'iems', 'euls', 'yas'*/
    ],
    il: [
      'ille',
      ...generateSuffixesListWithSeparators("il", "elle"),
      /*'ul', 'ol', 'yel',*/ 'iel', /*'el',*/ 'ielle', /*'im', 'iem', 'eul', 'ya', 'um', 'om', 'ax', 'ox'*/
    ],
    lui: ['ellui']
  };

  let generatedWordsRegexp = [];

  for(const [replaceWith, wordsToReplace] of Object.entries(wordFilters)) {
    let regexpStr = '';
    let i = 0;
    for(const word of wordsToReplace) {
      i++;
      regexpStr += startBoundary + word + endBoundary;
      regexpStr += i === wordsToReplace.length ? '' : '|';
    }
    generatedWordsRegexp[replaceWith] = XRegExp(regexpStr, 'mgiu');
  }

  return [
    [repeatPatternForAllSeparators('s,e,s', 's,es'), 's'],
    [generatedWordsRegexp['mien'], 'mien'],
    [generatedWordsRegexp['tous'], 'tous'],
    [generatedWordsRegexp['celui'], 'celui'],
    [generatedWordsRegexp['ceux'], 'ceux'],
    [generatedWordsRegexp['eux'], 'eux'],
    // [generatedWordsRegexp['mon'], 'mon'],
    // [generatedWordsRegexp['ton'], 'ton'],
    // [generatedWordsRegexp['son'], 'son'],
    [generatedWordsRegexp['ce'], 'ce'],
    [generatedWordsRegexp['un'], 'un'],
    // [generatedWordsRegexp['du'], 'du'],
    // [generatedWordsRegexp['le'], 'le'],
    [generatedWordsRegexp['ils'], 'ils'],
    [generatedWordsRegexp['il'], 'il'],
    [generatedWordsRegexp['lui'], 'lui'],
    [XRegExp(`(gentil${patternSeparators}le|gentils${patternSeparators}le)(-|⋅|·|\\.|)(?<!s|\\.(?=\\P{Latin}|$))`, 'mgiu'), 'gentil'],
    [XRegExp('copaine', 'mgiu'), 'copain'],
    [repeatPatternForAllSeparators('e,lle,au', 'e,au,lle', 'eau,elle'), 'eau'],
    [repeatPatternForAllSeparators('e,lles,aux', 'e,aux,lles', 'eaux,elles'), 'eaux'],
    [repeatPatternForAllSeparators('aux,ales', 'ales,aux'), 'aux'], // e.g. syndicaux-ales
    [repeatPatternForAllSeparators(',se,x', ',ses,x', ',x,se', ',x,ses'), 'x'],
    [repeatPatternForAllSeparators(',r,se', ',se,r', ',rs,se', ',se,rs'), 'r'],
    [XRegExp('eurice(?=\\P{Latin}|s|$)', 'mgiu'), 'eur'],
    //Points médians
    [XRegExp('(?<=\\p{Latin})(·|⋅|•|‧)[·⋅•‧A-Za-zÀ-ÿ]*(?<!s)', 'mgiu'), ''],
    //Point
    [XRegExp('(?<!^|www)(?!.*\\.(fr|com|eu|de|be|ca|ch|net|org|leclerc)(?=\\P{Latin}|$))\\.(?=e\\.s|e(?=\\P{Latin}|$)|le|fe|ve(?=\\P{Latin}|$)|trice|rice|ère|ere|ière|iere|ne(?=\\P{Latin}|$)|le(?=\\P{Latin}|$)|te|ale|lle|sse|se(?=\\P{Latin}|$)|es(?=\\P{Latin}|$))[.A-Za-zÀ-ÿ]*(?<!s|au\\W|x|\\.(?=\\P{Latin}|$))', 'mgiu'), ''],
    //Tiret
    [XRegExp('-(?!.*\\.(fr|com|eu|de|be|ca|ch|net|org|est)(?=\\P{Latin}|$))(?=e-|e(?=\\P{Latin}|$)|fe-s(?=\\P{Latin}|$)|fe(?=\\P{Latin}|$)|ve(?=\\P{Latin}|$)|trice|rice|ère|ere|ière|iere|ne(?=\\P{Latin}|$)|te-s(?=\\P{Latin}|$)|te(?=\\P{Latin}|$)|ale|lle|sse|se(?=\\P{Latin}|$)|es(?=\\P{Latin}|$))[-A-Za-zÀ-ÿ]*(?<!s|au\\W|x)', 'mgiu'), '']
  ];
}

export default generateReplaceEachRegexpsArray()
